<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __constrcut()
	{
		parent::__construct();
        $this->load->library(array('session', 'lib_login'));
	}
	
	 
	public function index()
	{
		$fb_data = $this->lib_login->facebook();
        // check login data
        if (isset($fb_data['me'])) {
            //var_dump($fb_data);
			//echo '<br><br>url:'.$fb_data['last_name'].'';
			//echo '<br><br>url:'.$fb_data['loginUrl'].'';
			//echo '<br><br>Firsname:'.$fb_data['me']['first_name'].'<br>';
			//echo '<br><br>Lastname:'.$fb_data['me']['last_name'].'<br>';
			echo '<a href="'.base_url().'auth/logout/facebook">Logout</a><br>';
			
			//$arr = array('a', 'b', 'c', 'd' , 'e');
			///echo json_encode($arr);
        } else {
            //echo '<a href="' . $fb_data['loginUrl'] . '">Login With Facebook</a>';
			redirect('auth/facebook');
        }
		/*$data = array(
			'' => '',
		);
		$this->load->view('../../app',$data);*/
		echo '';
	}
}
